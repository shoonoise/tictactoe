from core import TheGame

BOT_NAME = "AI"


class Bot:
    def __init__(self, enemy_side: int, game: TheGame):
        self.enemy_id = enemy_side
        self.game = game

    def move(self) -> tuple:
        state = self.game.state
        if state[1][1] is None:
            return 1, 1

        turned_field = tuple(zip(*state))

        positive_diag = 0
        negative_diag = 0

        for i in range(3):
            horizontal_match = tuple(filter(lambda x: x == self.enemy_id, state[i]))

            if len(horizontal_match) == 2:
                for j, v in enumerate(state[i]):
                    if j is None:
                        return i, j

            vertical_match = tuple(filter(lambda x: x == self.enemy_id, turned_field[i]))
            if len(vertical_match) == 2:
                for j, v in enumerate(turned_field[i]):
                    if v is None:
                        return j, i

            if state[i][i] == self.enemy_id:
                positive_diag += 1
                if positive_diag == 2:
                    for j in range(3):
                        if state[j][j] is None:
                            return j, j

            if state[i][2 - i] == self.enemy_id:
                negative_diag += 1
                if negative_diag == 2:
                    for j in range(3):
                        if state[j][2 - i] is None:
                            return j, j

        for i in range(3):
            try:
                j = state[i].index(None)
            except ValueError:
                pass
            else:
                return i, j
