import asyncio
import logging

OPCODES = {
    "START_GAME": 0,
    "END_GAME": 1,
    "REGISTRATION": 2,
    "MAKE_MOVE": 3,
    "GET_BOARD_STATE": 4,
    "GET_STAT": 5,
    "QUIT": 6,
    "NOP": 7,
}

STATUSES = {
    "OK": 0,
    "ERROR": 1,
}


async def read_response(reader, timeout=10):
    data = await asyncio.wait_for(reader.readline(), timeout=timeout)
    reply = data.decode()
    logger.debug("got from server: {!r}".format(reply))
    reply = reply.split(";")
    if len(reply) < 2:
        logger.error(reply)
        return

    status, msg = reply
    if int(status) == STATUSES["OK"]:
        return msg
    else:
        logger.error(msg)
        return


async def send_request(writer, opcode, *args):
    message = "{opcode};".format(opcode=opcode) + ";".join(args) + "\n"
    logger.debug("send to server: {!r}".format(message))
    writer.write(message.encode())


async def client(loop, server):
    reader, writer = await asyncio.open_connection(server, 8888, loop=loop)

    try:
        while True:
            command = input("Enter command [Registration/Game/Statistic/Move/Board/Quit]: ")

            if command == "R":
                name = input("Enter your name: ")
                await send_request(writer, OPCODES["REGISTRATION"], name)

                response = await read_response(reader)
                if response is None:
                    print("something went wrong, try again")
                    continue

                print(response)

            elif command == "G":
                await send_request(writer, OPCODES["START_GAME"])

                # Wait for matchmaking
                # TODO: helper
                while True:
                    data = await asyncio.wait_for(reader.readline(), timeout=10)
                    reply = data.decode()
                    if reply == "" or reply == "\n":
                        continue

                    print(reply)
                    status, msg = reply.split(";")
                    if int(status) == STATUSES["OK"]:
                        break

            elif command == "Q":
                await send_request(writer, OPCODES["QUIT"])
                response = await read_response(reader)
                print(response)
                break

            elif command == "M":
                h = input("h: ")
                v = input("v: ")
                await send_request(writer, OPCODES["MAKE_MOVE"], h, v)

                response = await read_response(reader)
                if response is None:
                    print("something went wrong, try again")
                    continue
                print(response)

            elif command == "B":
                await send_request(writer, OPCODES["GET_BOARD_STATE"])
                for i in range(4):
                    response = await read_response(reader)
                    if response is None:
                        print("something went wrong, try again")
                        break

                    print(response)

            elif command == "S":
                await send_request(writer, OPCODES["GET_STAT"])

                response = await read_response(reader)
                if response is None:
                    print("something went wrong, try again")
                    continue
                print(response)
            else:
                print("UNKNOWN COMMAND")
                continue

    except asyncio.futures.TimeoutError:
        logger.error("Server response too long, try to reconnect...")
    except Exception as e:
        logger.exception(e)
    finally:
        print('Close the socket')
        writer.close()


if __name__ == "__main__":
    common_loop = asyncio.get_event_loop()
    logger = logging.getLogger()
    logging.basicConfig(level=logging.ERROR)

    host = 'localhost'

    print("Welcome to Tic Tac Toe Game!")
    print("Try to connect to server {}...".format(host))

    try:
        common_loop.run_until_complete(client(common_loop, host))
    except KeyboardInterrupt:
        print("You stop the game.")
    finally:
        common_loop.close()
