##About
It's a simple multi-player tic-tac-toe game. 
 
##How to run
###Server
Just execute `python3 ./server.py`.

To see possible arguments run `python3 ./server.py --help`

###Client
Just execute `python3 ./client.py`

Command is a first letter of the full command name.
>For example, enter `R` for **R**egestration.


###Requirements
- Python 3.6

##Limitations and issues
- Executes in one thread.

> `gunicorn` is an option, but storage must be re-implemented (to make possible database share).

- The first move is always for the player which come first.

- The countdown is not very accurate

- A player have to poll server to get current game state

- A looser doesn't get message about loose

##TODO
- Tests

- Params (port, time for matchmaking)
