import dbm
import json


class UserAlreadyExists(BaseException):
    pass


class UserNotExists(BaseException):
    pass


class UserStorage:
    """
    A simple persistent data storage
    """

    def __init__(self):
        self._db = dbm.open("users", flag='c', mode=0o666)

    def new_user(self, name: str):
        if self._db.get(name, None) is not None:
            raise UserAlreadyExists
        self._db[name] = json.dumps({
            "games": 0,
            "win": 0,
            "loose": 0
        })

    def get_user_stat(self, name: str) -> dict:
        stat = self._db.get(name, None)
        if stat is None:
            raise UserNotExists(name)
        return json.loads(stat)

    def _save_win(self, name):
        current_stat = self.get_user_stat(name)
        current_stat["win"] += 1
        current_stat["games"] += 1
        self._db[name] = json.dumps(current_stat)

    def _save_loose(self, name):
        current_stat = self.get_user_stat(name)
        current_stat["loose"] += 1
        current_stat["games"] += 1
        self._db[name] = json.dumps(current_stat)

    def _save_draw(self, name):
        current_stat = self.get_user_stat(name)
        current_stat["games"] += 1
        self._db[name] = json.dumps(current_stat)

    def save_game(self, winner, looser, is_draw=False):
        if is_draw:
            self._save_draw(winner)
            self._save_draw(looser)
        else:
            self._save_win(winner)
            self._save_loose(looser)

    def close(self):
        self._db.sync()
        self._db.close()


if __name__ == "__main__":
    storage = UserStorage()
    # storage.new_user("test_1")
    # storage.new_user("test_2")
    print(storage.get_user_stat("test_1"))
    print(storage.get_user_stat("test_2"))
    storage.save_game("test_1", "test_2")
    print(storage.get_user_stat("test_1"))
    print(storage.get_user_stat("test_2"))

    storage.save_game("test_1", "test_2")
    print(storage.get_user_stat("test_1"))
    print(storage.get_user_stat("test_2"))
