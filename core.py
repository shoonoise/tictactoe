class DataError(BaseException):
    pass


class TheGame:
    X_SYMBOL = 0
    O_SYMBOL = 1

    def __init__(self, host_player):
        self._field_state = [[None, None, None],
                             [None, None, None],
                             [None, None, None]]
        self.current_turn = 0
        self.last_turn = None
        self.is_over = False
        self.winner = None
        self.host_player = host_player
        self.rival = None
        self.sides = {
            host_player: self.X_SYMBOL,
        }
        self._ready = False

    @classmethod
    def is_coord_valid(cls, coord: int) -> bool:
        if not isinstance(coord, int):
            return False
        if coord < 0 or coord > 2:
            return False
        return True

    def make_move(self, h: int, v: int, player_name: str):
        print(player_name, " player move. Current turn ", self.current_turn)
        side = self.sides[player_name]
        if self.last_turn == side:
            raise DataError("{} already made a move".format(player_name))
        if self.last_turn is None and side != self.X_SYMBOL:
            raise DataError("The first move must be done by {}".format(self.host_player))
        if self.is_coord_valid(h) and self.is_coord_valid(v):
            current = self._field_state[h][v]
            if current is not None:
                raise DataError("cell {}:{} already set".format(h, v))
            else:
                self._field_state[h][v] = side
                self.last_turn = side
                self.current_turn += 1
        else:
            raise DataError("invalid data")

    def is_game_over(self):
        if self.current_turn < 3:
            return False

        # it's a draw
        if self.current_turn == 7:
            self.is_over = True
            return True

        there_is_winner = False

        turned_field = tuple(zip(*self._field_state))
        positive_diag = 0
        negative_diag = 0
        for i in range(3):
            horizontal_match = tuple(filter(lambda x: x == self.last_turn, self._field_state[i]))
            if len(horizontal_match) == 3:
                there_is_winner = True
                break
            vertical_match = tuple(filter(lambda x: x == self.last_turn, turned_field[i]))
            if len(vertical_match) == 3:
                there_is_winner = True
                break

            if self._field_state[i][i] == self.last_turn:
                positive_diag += 1

            if self._field_state[i][2 - i] == self.last_turn:
                negative_diag += 1

        if positive_diag == 3 or negative_diag == 3:
            there_is_winner = True

        if there_is_winner:
            self.winner = self.last_turn
            self.is_over = True
            return True
        else:
            return False

    def __str__(self):
        return self.get_board().replace(";", "\n")

    def get_board(self) -> str:
        rep_map = {
            self.X_SYMBOL: "X",
            self.O_SYMBOL: "O",
            None: "."
        }
        result = ""
        for row in self._field_state:
            repr_row = map(lambda x: rep_map[x], row)
            result += "{0}\t{1}\t{2};".format(*repr_row)
        return result

    @property
    def state(self):
        # copy to avoid external change
        return self._field_state[:]

    @property
    def ready(self):
        return self._ready

    def join(self, name):
        if self.host_player is not None:
            self.sides[name] = self.O_SYMBOL
            self.rival = name
            self._ready = True
            return self
        else:
            raise DataError("game not started yet")


if __name__ == "__main__":
    game = TheGame("player one")
    game.join("player two")

    game.make_move(0, 2, "player one")
    game.make_move(1, 1, "player two")
    game.make_move(2, 0, "player one")
    print(game)
    print(game.is_game_over())
