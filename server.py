import argparse
import asyncio
import logging
import bot

from core import TheGame, DataError
from storage import UserStorage, UserAlreadyExists

OPCODES = {
    "START_GAME": 0,
    "END_GAME": 1,
    "REGISTRATION": 2,
    "MAKE_MOVE": 3,
    "GET_BOARD_STATE": 4,
    "GET_STAT": 5,
    "QUIT": 6,
    "NOP": 7,
}

STATUSES = {
    "OK": 0,
    "ERROR": 1,
}


class AlreadyInGameError(BaseException):
    pass


class Lobby:
    waiting = []
    registered_names = {bot.BOT_NAME}

    @classmethod
    def add_game(cls, game: TheGame):
        cls.waiting.append(game)

    @classmethod
    def add_player(cls, name: str):
        if name in cls.registered_names:
            raise AlreadyInGameError("%s already in a game")
        cls.registered_names.add(name)

    @classmethod
    def remove_user(cls, username: str):
        if username in cls.registered_names:
            cls.registered_names.remove(username)

    @classmethod
    def remove_game(cls, username: str):
        # FIXME: may cause performance issues if a lot of players
        for g in cls.waiting:
            if username == g.host_player:
                cls.waiting.remove(g)

    @classmethod
    def get_game(cls):
        if len(cls.waiting) > 0:
            return cls.waiting.pop(0)


async def notify(message, writer):
    raw_msg = "{};{}\n".format(STATUSES["OK"], message)
    logger.debug("write ok: {!r}".format(raw_msg))
    writer.write(raw_msg.encode())


async def notify_error(message, writer):
    raw_msg = "{};{}\n".format(STATUSES["ERROR"], message)
    logger.debug("write error: {!r}".format(raw_msg))
    writer.write(raw_msg.encode())


async def handle(reader, writer):
    addr = writer.get_extra_info('peername')

    board = None
    name = None
    ai = None
    rival = None

    try:
        while True:
            data = await asyncio.wait_for(reader.readline(), timeout=60 * 5)
            message = data.decode()
            if message == "":
                continue

            logger.debug("request from {}: {!r}".format(addr, message))

            opcode, args = int(message.split(";")[0]), message.split(";")[1:]

            if opcode == OPCODES["REGISTRATION"]:
                if name is not None:
                    await notify_error("You already registered, reconnect to change name", writer)
                    continue

                name = args[0].strip()
                try:
                    Lobby.add_player(name)
                except AlreadyInGameError:
                    await notify_error("You can't use the name. Name already in a game or it's reserved. ", writer)
                    name = None
                    continue
                else:
                    try:
                        storage.new_user(name)
                    except UserAlreadyExists:
                        pass
                    await notify("Welcome {}".format(name), writer)

            elif opcode == OPCODES["START_GAME"]:
                if board is not None and not board.is_game_over():
                    await notify_error("You already start the game", writer)
                    continue

                game = Lobby.get_game()
                if game is not None:
                    board = game.join(name)
                    rival = board.host_player
                    await notify("{}, you join in the game! Your rival is: {}.".format(name, rival), writer)
                    continue
                else:
                    board = TheGame(name)
                    Lobby.add_game(board)
                    counter = COUNTDOWN
                    await notify_error("Wait for matchmaking: {}s".format(counter), writer)
                    while True:
                        if not board.ready:
                            await asyncio.sleep(1)
                            counter -= 1
                            await notify_error("{}s".format(counter), writer)
                            if counter == 0:
                                Lobby.remove_game(name)
                                try:
                                    storage.new_user(bot.BOT_NAME)
                                except UserAlreadyExists:
                                    pass

                                ai = bot.Bot(TheGame.X_SYMBOL, board)
                                board.join(bot.BOT_NAME)
                                break
                        else:
                            break

                    rival = board.rival
                    await notify("Game starts, your rival is {}. Your move...".format(board.rival), writer)

            elif opcode == OPCODES["MAKE_MOVE"]:
                if board:
                    if board.ready:
                        try:
                            board.make_move(int(args[0]), int(args[1]), name)
                        except DataError as e:
                            logger.error("UserError: {}".format(e))
                            await notify_error(e, writer)
                            continue
                        else:
                            if board.is_game_over():
                                if board.winner is not None:
                                    logger.debug("There is a winner: {}".format(name))
                                    await notify("You are a winner!", writer)
                                    storage.save_game(name, rival)
                                else:
                                    logger.debug("It's a draw")
                                    await notify("A draw", writer)
                                    storage.save_game(name, rival, is_draw=True)

                                ai = None
                                rival = None

                                continue
                            else:
                                await notify("wait for {} turn...".format(rival), writer)

                        if ai is not None:
                            bot_move = ai.move()
                            if bot_move is None:
                                # FIXME: DRY
                                if board.is_game_over():
                                    if board.winner is not None:
                                        logger.debug("There is a winner: {}".format(name))
                                        await notify("You are a winner!", writer)
                                        storage.save_game(name, rival)
                                    else:
                                        logger.debug("It's a draw")
                                        await notify("Draw", writer)
                                        storage.save_game(name, rival, is_draw=True)

                                    ai = None
                                    rival = None
                                    storage.save_game(name, board.rival)
                                    break
                                else:
                                    logger.error("Bot unable to make a move")
                                    await notify_error("bot is broken :(", writer)
                                    continue

                            h, v = bot_move
                            logger.debug("bot going to move: {}:{}".format(h, v))
                            try:
                                board.make_move(h, v, bot.BOT_NAME)
                            except DataError as e:
                                logger.error("BotError: {}".format(e))
                                await notify_error(e, writer)
                                continue
                            else:
                                if board.is_game_over():
                                    logger.debug("There is a winner: {}".format("AI"))
                                    storage.save_game(board.rival, name)
                                    await notify("You loose :(", writer)
                                    storage.save_game(board.rival, name)
                                    break

                    else:
                        await notify("Waiting for rival...", writer)
                else:
                    await notify_error("The game is not active", writer)

            elif opcode == OPCODES["GET_STAT"]:
                if name is not None:
                    stat = storage.get_user_stat(name)
                    if stat["games"] > 0:
                        win_percent = round(float(stat["win"]) / float(stat["games"]) * 100, 1)
                        loose_percent = round(float(stat["loose"]) / float(stat["games"]) * 100, 1)
                        draw_percent = round(100 - (win_percent + loose_percent))
                        draw = stat["games"] - (stat["win"] + stat["loose"])
                        stat.update({"win_percent": win_percent,
                                     "loose_percent": loose_percent,
                                     "draw_percent": draw_percent,
                                     "draw": draw})
                    else:
                        await notify("no games yet", writer)
                        continue

                    await notify("win: {win} ({win_percent}%), loose: {loose} ({loose_percent}%), draw: {draw} ("
                                 "{draw_percent}%)".format(**stat), writer)
                else:
                    await notify_error("You must to login to see statistic", writer)

            elif opcode == OPCODES["GET_BOARD_STATE"]:
                if board is not None:
                    for line in board.get_board().split(";"):
                        await notify(line, writer)
                else:
                    await notify_error("No active game", writer)

            elif opcode == OPCODES["QUIT"]:
                await notify("See you", writer)
                break

            else:
                print("unknown opcode: ", opcode)

    except ConnectionResetError:
        logger.info("User %s closes connection", name)
    except asyncio.futures.TimeoutError:
        # TODO: force surrender?
        logger.info("Client idle too long, drop the connection")
    except Exception as e:
        logger.exception(e)
    finally:
        Lobby.remove_game(name)
        Lobby.remove_user(name)
        logger.debug("Close the client socket")
        writer.close()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--countdown', type=int, help='time before matchmaking with AI triggered')
    args = parser.parse_args()
    COUNTDOWN = args.countdown

    logger = logging.getLogger()
    logging.basicConfig(level=logging.DEBUG)

    storage = UserStorage()

    loop = asyncio.get_event_loop()
    handler = asyncio.start_server(handle, '127.0.0.1', 8888, loop=loop)
    server = loop.run_until_complete(handler)

    # Serve requests until Ctrl+C is pressed
    print('Serving on {}'.format(server.sockets[0].getsockname()))
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        server.close()
        loop.run_until_complete(server.wait_closed())
        loop.close()
